import { IResolvers, makeExecutableSchema } from 'graphql-tools';
import SkillSchema from './skill.schema.graphql';
import SkillResolver from './skill.resolver';

export const SkillExecSchema = makeExecutableSchema({
    typeDefs: SkillSchema,
    resolvers: SkillResolver as IResolvers
});

export default SkillExecSchema;
