import { IResolvers, makeExecutableSchema } from 'graphql-tools';
import CharacterSchema from './character.schema.graphql';
import CharacterResolver from './character.resolver';

export const CharaterExecSchema = makeExecutableSchema({
    typeDefs: CharacterSchema,
    resolvers: CharacterResolver as IResolvers
});

export default CharaterExecSchema;
