import { mergeSchemas } from "graphql-tools";
import CharacterExecSchema from "./character/character.module";
import SkillExecSchema from "./skill/skill.module";

export default mergeSchemas({
  schemas: [
        CharacterExecSchema,
        SkillExecSchema,
  ],
});
