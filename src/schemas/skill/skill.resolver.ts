import { Skill, Resolvers, SkillWeight } from '../../types';
import Db from '../database'
const db = new Db<Skill>('skills');

const skillResolver = {
    Query: {
        async skills(): Promise<Skill[]> {
            const result = await db.fetchAll();
            return Object.entries(result.data).map(([id, data]: [string, any]) => ({id, ...data}));
        },

        async skill(_, { id }): Promise<Skill|null> {
            return db.fetchOne(id);
        }
    },

    Mutation: {
        async createSkill(_, {args}): Promise<Skill> {
            return db.create(args);
        },

        async updateSkill(_, {id, args}): Promise<boolean> {
            console.log(id)
            console.log(args)

            return db.update(id, args);
        },

        async deleteSkill(_, {id}): Promise<boolean> {
            return db.delete(id);
        }
    },

    Skill: {
        // Need to cast parent to any since Skill.SkillWeight.Skill is of type Skill.
        // This way we can use the id that is returned from the db without the 
        // ts-engine going bananas over us
        async skillWeigths(parent: any): Promise<SkillWeight[]> {
            const result = await db.fetchAll();
            return parent.skillWeigths.map((sw:any) => ({...sw, skill: result[sw.skill]} as SkillWeight) )
        }
    }
} as Resolvers;

export default skillResolver;
