const fs= require('fs');
const {execSync} = require('child_process');

if (!process.argv[2]) {
    console.log("No name provided. Exiting.");
    process.exit(1)
}

const moduleName = process.argv[2];
const capitalName = moduleName.charAt(0).toUpperCase() + moduleName.slice(1);
const path = `./src/schemas/${moduleName}`;
if (!fs.existsSync(path)) {
    fs.mkdirSync(path);
    console.log(`Created ${path}`);
}

const exportPath = `${path}/${moduleName}.module.ts`
const schemasPath = `${path}/${moduleName}.schema.graphql`
const resolversPath = `${path}/${moduleName}.resolver.ts`

const database = './src/schemas/database.ts';
const schemasIndex = './src/schemas/index.ts';

const schemaTemplate = `
type Query {
    ${moduleName}(id: ID!): ${capitalName}
    ${moduleName}s: [${capitalName}]
}

type Mutation {
    create${capitalName}(args:Create${capitalName}Input): ${capitalName}!
    update${capitalName}(args:Update${capitalName}Input): Boolean!
    delete${capitalName}(id: ID!): Boolean!'
}

type ${capitalName} {
    id: ID!
}

input Create${capitalName}Input {
}

input Update${capitalName}Input {
    id: ID!
}
`

const resolverTemplate = 
`import Db from './database'
const db = new Db<${capitalName}>('${moduleName}s');

const ${moduleName}Resolver = {
    Query: {
        async ${moduleName}s(): Promise<${capitalName}[]> {
            return await db.fetchAll();
        },

        async ${moduleName}(_: any, { id }: {id: string} ): Promise<${capitalName}> {
            return db.fetchOne(id);
        }
    },

    Mutation: {
        async create${capitalName}(root:any, args:any): Promise<${capitalName}> {
            return db.create(args);
        },


        async update${capitalName}(root:any, args:any): Promise<${capitalName}> {
            return db.update(args);
        },

        async delete${capitalName}(root: any, {id}: {id: string}) {
            return db.delete(id);
        }
    }
};

export default ${moduleName}Resolver;
`

const moduleTemplate = 
`import { IResolvers, makeExecutableSchema } from 'graphql-tools';
import ${capitalName}Schema from '../schemas/${moduleName}.schema.graphql';
import ${capitalName}Resolver from '../resolvers/${moduleName}.resolver';

export const ${capitalName}Module = makeExecutableSchema({
    typeDefs: ${capitalName}Schema,
    resolvers: ${capitalName}Resolver as IResolvers
});

export default ${capitalName}Module;
`

fs.writeFileSync(schemasPath, schemaTemplate);
console.log(`Wrote: ${schemasPath}`)
fs.writeFileSync(exportPath, moduleTemplate);
console.log(`Wrote: ${exportPath}`)
fs.writeFileSync(resolversPath, resolverTemplate);
console.log(`wrote: ${resolversPath}`)

try {
    execSync(`sed -Ei "/type ResourceName/ s/;$/ | '${moduleName}s';/" ${database}`)
    console.log(`Added "${moduleName}" to database types`)

    execSync(`sed -Ei "1 a import ${capitalName}ExecSchema from '.\/${moduleName}\/${moduleName}.module'" ${schemasIndex}`)
    execSync(`sed -Ei '/schemas: / a ${capitalName}ExecSchema,' ${schemasIndex}`)
    console.log(`Added ${capitalName}ExecSchema to schemas/index.ts`)
} catch (e) {
    console.log(String(e.output[0]))
}
