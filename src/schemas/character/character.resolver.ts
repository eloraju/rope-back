import { Character, Resolvers } from '../../types';
import Db from '../database'

const db = new Db<Character>('characters');

const characterResolver: Resolvers = {
    Query: {
        async characters(): Promise<Character[]> {
            const result = await db.fetchAll();
            return Object.entries(result.data).map(([id, data]: [string, any]) => ({id, ...data}));
        },

        async character(_, { charId } ): Promise<Character|null> {
            return db.fetchOne(charId);
        }
    },

    Mutation: {
        async createCharacter(_, {args}): Promise<Character> {
            return db.create(args);
        },

        async updateCharacter(_, {id, args}): Promise<boolean> {
            return db.update(id, args);
        },

        async deleteCharacter(_, {id}): Promise<boolean> {
            return db.delete(id);
        }
    }
};

export default characterResolver;
