import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { createServer } from 'http';
import compression from 'compression';
import cors from 'cors';
import 'graphql-import-node';
import mergedSchema from './schemas';

const schema = mergedSchema;

const app = express();
const server = new ApolloServer({
    schema
});

app.use('*', cors());
app.use(compression());
server.applyMiddleware({ app, path: '/graphql' });

const httpServer = createServer(app);
httpServer.listen(
  { port: 3000 },
  (): void => console.log(`DB_URL: ${process.env.DB_URL}\nGraphQL is now running on http://localhost:3000/graphql`));
