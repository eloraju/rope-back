import axios, { AxiosInstance } from 'axios';

const instance = axios.create({
    baseURL: process.env.DB_URL
});

instance.interceptors.request.use(conf => {
        console.log(`Making a request to  ${conf.url}`)
        return conf;
    });

export type ResourceName = 'characters' | 'skills' | 'items';

export default class Db<T> {
    private resource: ResourceName;
    private axios: AxiosInstance;

    constructor(resource: ResourceName) {
        this.resource = resource;
        this.axios = axios.create({baseURL: process.env.DB_URL});
    }

    async fetchOne(id: string): Promise<T|null> {
        try {
            const result = await this.axios.get(`${this.resource}/${id}.json`)
            return {id, ...result.data};
        } catch(e) {
            console.log(e)
            return null;
        }
        
    }

    async fetchAll(): Promise<{[id: string]: T}> {
        try {
            return (await this.axios.get(`${this.resource}.json`)).data
        } catch (e) {
            console.log(e);
            return {};
        }
    }

    async create(args: any): Promise<T> {
        try {
            const result = await this.axios.post(`${this.resource}.json`, args);
            return {id: result.data.name, ...args};
        } catch (e) {
            console.log(e);
            throw e;
        }
    }

    async update(id: string, args: any): Promise<boolean> {
        try {
        return !!await this.axios.patch(`${this.resource}/${id}.json`, args);
        } catch (e) {
            console.log(e)
            return false
        }
    }

    async delete(id: string): Promise<boolean> {
        return !!await this.axios.delete(`${this.resource}/${id}.json`)
    }
};

